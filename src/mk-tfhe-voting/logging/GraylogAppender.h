//
// Created by Lukas on 17.11.2019.
//

#ifndef GRAYLOG_GRAYLOGAPPENDER_H
#define GRAYLOG_GRAYLOGAPPENDER_H

#include <log4cplus/appender.h>
#include <vector>
#include <log4cplus/spi/loggingevent.h>
#include <boost/asio/io_service.hpp>
//#include <gelfcpp/GelfMessageStream.hpp>
#include <gelfcpp/output/GelfUDPOutput.hpp>
#include <gelfcpp/decorator/Timestamp.hpp>

class GraylogAppender : public log4cplus::Appender {
private:
	gelfcpp::output::GelfUDPOutput graylog;
	gelfcpp::decorator::CurrentTimestamp timestamp;

	std::string application;
	std::string environment;
	std::string host;

	int mapLogLevel(int level);

public:
	GraylogAppender(std::string hostname, unsigned short port, std::string application, std::string environment, std::string selfhost);

	void close() override;

protected:
	void append(const log4cplus::spi::InternalLoggingEvent &event) override;
};

#endif //GRAYLOG_GRAYLOGAPPENDER_H
