#include "LoggerFactory.h"
#include "GraylogAppender.h"

#include <log4cplus/helpers/property.h>

namespace logger {
	log4cplus::Logger getDefaultLogger() {
		log4cplus::PropertyConfigurator config{"config/log4cplus.properties"};
		config.configure();
		log4cplus::Logger logger = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("main"));

		const log4cplus::helpers::Properties &properties{config.getProperties()};
		bool enabled{true};
		properties.getBool(enabled, "appender.Graylog.enabled");
		if (enabled) {
			std::string hostname{"138.201.127.240"};
			int port{12201};
			std::string application{"Trustee"};
			std::string environment{"DEV"};
			std::string selfhost{"localhost"};

			hostname = properties.getProperty("appender.Graylog.hostname");
			properties.getInt(port, "appender.Graylog.port");
			application = properties.getProperty("appender.Graylog.application");
			environment = properties.getProperty("appender.Graylog.environment");
			selfhost = properties.getProperty("appender.Graylog.selfhost");

			log4cplus::helpers::SharedObjectPtr<log4cplus::Appender> graylogAppender{new GraylogAppender{hostname, static_cast<unsigned short>(port), application, environment, selfhost}};
			graylogAppender->setName("GraylogAppender");
			logger.addAppender(graylogAppender);
		}

		return logger;
	}
}