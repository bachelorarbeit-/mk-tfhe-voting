#ifndef MK_TFHE_VOTING_LOGGERFACTORY_H
#define MK_TFHE_VOTING_LOGGERFACTORY_H

#include <log4cplus/configurator.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/spi/factory.h>

#include <mk-tfhe-voting/logging/GraylogAppender.h>
#include <mk-tfhe-voting/logging/LoggerFactory.h>

namespace logger {
	log4cplus::Logger getDefaultLogger();
}


#endif //MK_TFHE_VOTING_LOGGERFACTORY_H
