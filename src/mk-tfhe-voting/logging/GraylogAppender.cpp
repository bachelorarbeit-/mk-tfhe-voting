#include "GraylogAppender.h"
#include <log4cplus/loglevel.h>
#include <gelfcpp/GelfMessageStream.hpp>

GraylogAppender::GraylogAppender(std::string hostname, unsigned short port, std::string application, std::string environment, std::string selfhost) : graylog{hostname, port}, timestamp{},
																																					  application{application},
																																					  environment{environment},
																																					  host{selfhost} {

}

int GraylogAppender::mapLogLevel(int level) {

	switch (level) {
		case log4cplus::OFF_LOG_LEVEL:
			return 7;
		case log4cplus::FATAL_LOG_LEVEL:
			return 0;
		case log4cplus::ERROR_LOG_LEVEL:
			return 3;
		case log4cplus::WARN_LOG_LEVEL:
			return 4;
		case log4cplus::INFO_LOG_LEVEL:
			return 6;
		case log4cplus::DEBUG_LOG_LEVEL:
			return 7;
		case log4cplus::TRACE_LOG_LEVEL:
			return 7;
		default:
			return 3;
	}
}

void GraylogAppender::close() {
	this->closed = true;
}

void GraylogAppender::append(const log4cplus::spi::InternalLoggingEvent &event) {
	GELF_MESSAGE(graylog)
				("host", this->host)
						(timestamp)
						(event.getMessage())
						("application", this->application)
						("environment", this->environment)
						("facility", event.getLoggerName())
						("file", event.getFile())
						("function", event.getFunction())
						("line", event.getLine())
						("level", this->mapLogLevel(event.getLogLevel()))
						("thread", event.getThread());
}


