#ifndef MK_TFHE_VOTING_DATAREPOSITORYHELPERS_H
#define MK_TFHE_VOTING_DATAREPOSITORYHELPERS_H

#include "DataRepository.h"

namespace hsr::data::helpers {
	int callback(void *pointer, int length, char **values, char **);

	long getLastRowId(sqlite3 *db, const std::string &table);

	template<typename value_type>
	void storeVector(DataRepository &repo, sqlite3 *db, std::vector<value_type> &vector, const std::string &tableName, const std::vector<std::string> &fieldnames,
					 std::function<void(sqlite3_stmt *stmt, value_type &value, long id)> callback) {
		try {
			char *errorMessage = nullptr;

			sqlite3_stmt *stmt;
			const char *pzTest;

			repo.transactionBegin();

			long id = getLastRowId(db, tableName);

			std::string fieldNameString{};
			std::string valuePlaceholderString{};
			for (const std::string &fieldName : fieldnames) {
				if (fieldNameString.length() == 0) {
					fieldNameString.append(fieldName);
					valuePlaceholderString.append("?");
				} else {
					fieldNameString.append(",");
					fieldNameString.append(fieldName);
					valuePlaceholderString.append(",?");
				}
			}
			std::string sql{"INSERT INTO " + tableName + "(" + fieldNameString + ") VALUES (" + valuePlaceholderString + "); "};

			if (sqlite3_prepare(db, sql.c_str(), sql.size(), &stmt, &pzTest) == SQLITE_OK) {
				for (value_type &value : vector) {
					long newId = ++id;
					callback(stmt, value, newId);

					int retVal = sqlite3_step(stmt);
					if (retVal != SQLITE_DONE) {
						std::string error{"\nCommit Failed! " + std::to_string(retVal)};
						throw std::runtime_error{error};
					}

					sqlite3_reset(stmt);
				}
				sqlite3_finalize(stmt);
			} else {
				std::string error{"SQL error: "};
				error += sqlite3_errmsg(db);
				sqlite3_free(errorMessage);
				throw std::runtime_error{error};
			}

			repo.transactionCommit();
		} catch (std::runtime_error &e) {
			repo.transactionRollback();
			throw e;
		}
	}

	template<typename value_type>
	void select(sqlite3 *db, const std::string &select, std::vector<value_type> &vector, std::function<value_type(sqlite3_stmt *stmt)> callback) {
		sqlite3_stmt *statement;
		const std::string sql{select};
		if (sqlite3_prepare(db, sql.c_str(), sql.length(), &statement, nullptr) != SQLITE_OK) {
			throw std::runtime_error{select + " failed"};
		}

		while (sqlite3_step(statement) == SQLITE_ROW) {
			vector.push_back(callback(statement));
		}
		sqlite3_finalize(statement);
	}

	template<typename value_type>
	void findAll(sqlite3 *db, std::vector<value_type> &vector, const std::string &tableName, std::function<value_type(sqlite3_stmt *stmt)> callback) {
		select(db, std::string{"SELECT * FROM " + tableName}, vector, callback);
	}

	namespace to_stream {
		std::ostream &operator<<(std::ostream &os, std::shared_ptr<MKLweSample> &mkLweSample);
	}
}

#endif //MK_TFHE_VOTING_DATAREPOSITORYHELPERS_H
