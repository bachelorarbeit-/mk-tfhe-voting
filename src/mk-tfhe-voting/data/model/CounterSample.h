#ifndef MK_TFHE_VOTING_COUNTERSAMPLE_H
#define MK_TFHE_VOTING_COUNTERSAMPLE_H

namespace hsr::data::model {
	class CounterSample {
		unsigned long id;
		unsigned long vote_execution_id;
		unsigned long choice_id;
		unsigned int order_id;
		unsigned long lwe_public_key_id;

	public:
		CounterSample(unsigned long id, unsigned long voteExecutionId, unsigned long choiceId, unsigned int orderId, unsigned long lwePublicKeyId);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned long getVoteExecutionId() const;

		void setVoteExecutionId(unsigned long voteExecutionId);

		unsigned long getChoiceId() const;

		void setChoiceId(unsigned long choiceId);

		unsigned int getOrderId() const;

		void setOrderId(unsigned int orderId);

		unsigned long getLwePublicKeyId() const;

		void setLwePublicKeyId(unsigned long lwePublicKeyId);
	};
}

#endif //MK_TFHE_VOTING_COUNTERSAMPLE_H
