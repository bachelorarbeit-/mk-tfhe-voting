#include "CounterSample.h"

namespace hsr::data::model {
	CounterSample::CounterSample(unsigned long id, unsigned long voteExecutionId, unsigned long choiceId, unsigned int orderId, unsigned long lwePublicKeyId) : id(id), vote_execution_id(voteExecutionId), choice_id(choiceId),
																																								order_id(orderId),
																																								lwe_public_key_id(lwePublicKeyId) {}

	unsigned long CounterSample::getId() const {
		return id;
	}

	void CounterSample::setId(unsigned long id) {
		CounterSample::id = id;
	}

	unsigned long CounterSample::getVoteExecutionId() const {
		return vote_execution_id;
	}

	void CounterSample::setVoteExecutionId(unsigned long voteExecutionId) {
		vote_execution_id = voteExecutionId;
	}

	unsigned long CounterSample::getChoiceId() const {
		return choice_id;
	}

	void CounterSample::setChoiceId(unsigned long choiceId) {
		choice_id = choiceId;
	}

	unsigned int CounterSample::getOrderId() const {
		return order_id;
	}

	void CounterSample::setOrderId(unsigned int orderId) {
		order_id = orderId;
	}

	unsigned long CounterSample::getLwePublicKeyId() const {
		return lwe_public_key_id;
	}

	void CounterSample::setLwePublicKeyId(unsigned long lwePublicKeyId) {
		lwe_public_key_id = lwePublicKeyId;
	}
}