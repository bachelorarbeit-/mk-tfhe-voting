#include "Vote.h"

namespace hsr::data::model {
	Vote::Vote(unsigned long id, unsigned long externalId, bool ready, bool processed, std::string hash) : id{id}, externalId(externalId), ready{ready}, processed{processed}, hash{hash} {}

	unsigned long Vote::getId() const {
		return id;
	}

	void Vote::setId(unsigned long id) {
		Vote::id = id;
	}

	unsigned long Vote::getExternalId() const {
		return externalId;
	}

	void Vote::setExternalId(unsigned long externalId) {
		Vote::externalId = externalId;
	}

	bool Vote::isReady() const {
		return ready;
	}

	void Vote::setReady(bool ready) {
		Vote::ready = ready;
	}

	bool Vote::isProcessed() const {
		return processed;
	}

	void Vote::setProcessed(bool processed) {
		Vote::processed = processed;
	}

	const std::string &Vote::getHash() const {
		return hash;
	}

	void Vote::setHash(const std::string &hash) {
		Vote::hash = hash;
	}
}