#include <lwesamples.h>

#include <utility>
#include <memory>
#include "LwePublicKey.h"

namespace hsr::data::model {

	LwePublicKey::LwePublicKey(const MKLweSample &sample) : id{0}, length{static_cast<unsigned int>(sample.n * sample.parties)}, phaseShift{std::vector<int>{sample.a, sample.a + sample.n * sample.parties}}, phase{sample.b} {}

	LwePublicKey::LwePublicKey(const LweSample &sample, unsigned int length) : id{0}, length{length}, phaseShift{std::vector<int>{sample.a, sample.a + length}}, phase{sample.b} {}

	LwePublicKey::LwePublicKey(unsigned long id, unsigned int length, std::vector<int> phaseShift, int phase) : id{id}, length{length}, phaseShift{std::move(phaseShift)}, phase{phase} {}

	unsigned long LwePublicKey::getId() const {
		return id;
	}

	void LwePublicKey::setId(unsigned long id) {
		LwePublicKey::id = id;
	}

	unsigned int LwePublicKey::getLength() const {
		return length;
	}

	void LwePublicKey::setLength(unsigned int length) {
		LwePublicKey::length = length;
	}

	const std::vector<int> &LwePublicKey::getPhaseShift() const {
		return phaseShift;
	}

	void LwePublicKey::setPhaseShift(const std::vector<int> &phaseShift) {
		LwePublicKey::phaseShift = phaseShift;
	}

	int LwePublicKey::getPhase() const {
		return phase;
	}

	void LwePublicKey::setPhase(int phase) {
		LwePublicKey::phase = phase;
	}

	bool LwePublicKey::operator==(hsr::data::model::LwePublicKey const &right) const {
		return this->getId() == right.getId() && this->getLength() == right.getLength() && this->getPhase() == right.getPhase() && this->getPhaseShift() == right.getPhaseShift();
	}

	std::shared_ptr<MKLweSample> LwePublicKey::toMKLweSample(const LweParams *lweParams, const MKTFHEParams *MktfheParams) {
		std::shared_ptr<MKLweSample> samlpe{std::make_shared<MKLweSample>(lweParams, MktfheParams)};

		samlpe->b = this->phase;
		std::copy(std::begin(this->phaseShift), std::end(this->phaseShift), samlpe->a);

		return samlpe;
	}
}