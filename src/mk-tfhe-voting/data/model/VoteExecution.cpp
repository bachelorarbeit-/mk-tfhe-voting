#include "VoteExecution.h"

namespace hsr::data::model {

	VoteExecution::VoteExecution(unsigned long id, unsigned int orderId, unsigned int choices) : id(id), orderId(orderId), choices(choices) {}

	unsigned long VoteExecution::getId() const {
		return this->id;
	}

	void VoteExecution::setId(unsigned long id) {
		this->id = id;
	}

	unsigned int VoteExecution::getOrderId() const {
		return this->orderId;
	}

	void VoteExecution::setOrderId(unsigned int orderId) {
		this->orderId = orderId;
	}

	unsigned int VoteExecution::getChoices() const {
		return this->choices;
	}

	void VoteExecution::setChoices(unsigned int choices) {
		this->choices = choices;
	}
}