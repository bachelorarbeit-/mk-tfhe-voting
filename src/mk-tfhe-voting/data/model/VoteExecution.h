#ifndef MK_TFHE_VOTING_VOTEEXECUTION_H
#define MK_TFHE_VOTING_VOTEEXECUTION_H

namespace hsr::data::model {
	class VoteExecution {
		unsigned long id;
		unsigned int orderId;
		unsigned int choices;

	public:
		VoteExecution(unsigned long id, unsigned int orderId, unsigned int choices);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned int getOrderId() const;

		void setOrderId(unsigned int orderId);

		unsigned int getChoices() const;

		void setChoices(unsigned int choices);
	};
}


#endif //MK_TFHE_VOTING_VOTEEXECUTION_H
