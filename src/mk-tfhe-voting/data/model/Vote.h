#ifndef MK_TFHE_VOTING_VOTE_H
#define MK_TFHE_VOTING_VOTE_H

#include <string>

namespace hsr::data::model {
	class Vote {
		unsigned long id;
		unsigned long externalId;
		bool ready;
		bool processed;
		std::string hash;

	public:
		Vote(unsigned long id, unsigned long externalId, bool ready, bool processed, std::string hash);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned long getExternalId() const;

		void setExternalId(unsigned long externalId);

		bool isReady() const;

		void setReady(bool ready);

		bool isProcessed() const;

		void setProcessed(bool processed);

		const std::string &getHash() const;

		void setHash(const std::string &hash);
	};
}

#endif //MK_TFHE_VOTING_VOTE_H
