#ifndef MK_TFHE_VOTING_VOTESAMPLE_H
#define MK_TFHE_VOTING_VOTESAMPLE_H

namespace hsr::data::model {
	class VoteSample {
		unsigned long id;
		unsigned long vote_id;
		unsigned long vote_execution_id;
		unsigned int order_id;
		unsigned long lwe_public_key_id;

	public:
		VoteSample(unsigned long id, unsigned long voteId, unsigned long voteExecutionId, unsigned int orderId, unsigned long lwePublicKeyId);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned long getVoteId() const;

		void setVoteId(unsigned long voteId);

		unsigned long getVoteExecutionId() const;

		void setVoteExecutionId(unsigned long voteExecutionId);

		unsigned int getOrderId() const;

		void setOrderId(unsigned int orderId);

		unsigned long getLwePublicKeyId() const;

		void setLwePublicKeyId(unsigned long lwePublicKeyId);
	};
}


#endif //MK_TFHE_VOTING_VOTESAMPLE_H
