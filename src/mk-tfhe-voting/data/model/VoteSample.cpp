#include "VoteSample.h"

namespace hsr::data::model {

	VoteSample::VoteSample(unsigned long id, unsigned long voteId, unsigned long voteExecutionId, unsigned int orderId, unsigned long lwePublicKeyId) : id(id), vote_id(voteId), vote_execution_id(voteExecutionId),
																																						order_id(orderId), lwe_public_key_id(lwePublicKeyId) {}

	unsigned long VoteSample::getId() const {
		return id;
	}

	void VoteSample::setId(unsigned long id) {
		VoteSample::id = id;
	}

	unsigned long VoteSample::getVoteId() const {
		return vote_id;
	}

	void VoteSample::setVoteId(unsigned long voteId) {
		vote_id = voteId;
	}

	unsigned long VoteSample::getVoteExecutionId() const {
		return vote_execution_id;
	}

	void VoteSample::setVoteExecutionId(unsigned long voteExecutionId) {
		vote_execution_id = voteExecutionId;
	}

	unsigned int VoteSample::getOrderId() const {
		return order_id;
	}

	void VoteSample::setOrderId(unsigned int orderId) {
		order_id = orderId;
	}

	unsigned long VoteSample::getLwePublicKeyId() const {
		return lwe_public_key_id;
	}

	void VoteSample::setLwePublicKeyId(unsigned long lwePublicKeyId) {
		lwe_public_key_id = lwePublicKeyId;
	}
}