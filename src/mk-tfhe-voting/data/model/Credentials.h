#ifndef MK_TFHE_VOTING_CREDENTIALS_H
#define MK_TFHE_VOTING_CREDENTIALS_H

#include <string>

namespace hsr::data::model {
	class Credentials {
	private:
		unsigned long id;
		unsigned long componentId;
		std::string apikey;
	public:
		Credentials(unsigned long id, unsigned long componentId, const std::string &apikey);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned long getComponentId() const;

		void setComponentId(unsigned long componentId);

		const std::string &getApikey() const;

		void setApikey(const std::string &apikey);
	};
}


#endif //MK_TFHE_VOTING_CREDENTIALS_H
