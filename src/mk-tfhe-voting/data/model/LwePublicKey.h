#ifndef MK_TFHE_VOTING_LWEPUBLICKEY_H
#define MK_TFHE_VOTING_LWEPUBLICKEY_H

#include <vector>
#include <sqlite3.h>
#include <mkTFHEsamples.h>
#include <memory>

namespace hsr::data::model {
	class LwePublicKey {
	private:
		unsigned long id;
		unsigned int length;
		std::vector<int> phaseShift;
		int phase;

	public:
		explicit LwePublicKey(const MKLweSample &sample);

		explicit LwePublicKey(const LweSample &sample, unsigned int length);

		LwePublicKey(unsigned long id, unsigned int length, std::vector<int> phaseShift, int phase);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned int getLength() const;

		void setLength(unsigned int length);

		const std::vector<int> &getPhaseShift() const;

		void setPhaseShift(const std::vector<int> &phaseShift);

		int getPhase() const;

		void setPhase(int phase);

		bool operator==(LwePublicKey const &right) const;

		std::shared_ptr<MKLweSample> toMKLweSample(const LweParams *pParams, const MKTFHEParams *pMktfheParams);
	};
}


#endif //MK_TFHE_VOTING_LWEPUBLICKEY_H
