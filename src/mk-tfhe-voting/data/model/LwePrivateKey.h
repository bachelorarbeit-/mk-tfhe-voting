#ifndef MK_TFHE_VOTING_LWEPRIVATEKEY_H
#define MK_TFHE_VOTING_LWEPRIVATEKEY_H

#include <vector>
#include <lwekey.h>

namespace hsr::data::model {
	class LwePrivateKey {
	private:
		unsigned long id;
		unsigned int length;
		std::vector<std::byte> keyBits;
	public:
		explicit LwePrivateKey(LweKey &lweKey);

		LwePrivateKey(unsigned long id, unsigned int length, const std::vector<std::byte> &keyBits);

		unsigned long getId() const;

		void setId(unsigned long id);

		unsigned int getLength() const;

		void setLength(unsigned int length);

		const std::vector<std::byte> &getKeyBits() const;

		void setKeyBits(const std::vector<std::byte> &keyBits);

		bool operator==(LwePrivateKey const &other) const;

		void toLweKey(LweKey &lweKey) const;
	};
}


#endif //MK_TFHE_VOTING_LWEPRIVATEKEY_H
