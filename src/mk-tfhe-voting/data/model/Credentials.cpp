#include "Credentials.h"

namespace hsr::data::model {
	Credentials::Credentials(unsigned long id, unsigned long componentId, const std::string &apikey) : id(id), componentId(componentId), apikey(apikey) {}

	unsigned long Credentials::getId() const {
		return id;
	}

	void Credentials::setId(unsigned long id) {
		Credentials::id = id;
	}

	unsigned long Credentials::getComponentId() const {
		return componentId;
	}

	void Credentials::setComponentId(unsigned long componentId) {
		Credentials::componentId = componentId;
	}

	const std::string &Credentials::getApikey() const {
		return apikey;
	}

	void Credentials::setApikey(const std::string &apikey) {
		Credentials::apikey = apikey;
	}
}