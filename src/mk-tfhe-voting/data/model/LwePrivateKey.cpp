#include <lwekey.h>
#include <lweparams.h>
#include <cstddef>
#include <stdexcept>
#include "LwePrivateKey.h"

namespace hsr::data::model {
	LwePrivateKey::LwePrivateKey(LweKey &lweKey) : id{}, length{static_cast<unsigned int>(lweKey.params->n)}, keyBits{} {
		for (unsigned int i = 0; i < (length / 8 + 1); i++) {
			std::byte keyBit{0};
			for (unsigned int b{}; b < 8; b++) {
				unsigned int keyIndex{8 * i + b};
				if (keyIndex < length) {
					keyBit |= static_cast<std::byte>(lweKey.key[keyIndex] << b);
				}
			}
			keyBits.push_back(keyBit);
		}
	}

	LwePrivateKey::LwePrivateKey(unsigned long id, unsigned int length, const std::vector<std::byte> &keyBits) : id(id), length(length), keyBits(keyBits) {}

	unsigned long LwePrivateKey::getId() const {
		return id;
	}

	void LwePrivateKey::setId(unsigned long id) {
		LwePrivateKey::id = id;
	}

	unsigned int LwePrivateKey::getLength() const {
		return length;
	}

	void LwePrivateKey::setLength(unsigned int length) {
		LwePrivateKey::length = length;
	}

	const std::vector<std::byte> &LwePrivateKey::getKeyBits() const {
		return keyBits;
	}

	void LwePrivateKey::setKeyBits(const std::vector<std::byte> &keyBits) {
		LwePrivateKey::keyBits = keyBits;
	}

	bool LwePrivateKey::operator==(LwePrivateKey const &other) const {
		return this->getId() == other.getId() && this->getLength() == other.getLength() && this->getKeyBits() == other.getKeyBits();
	}

	void LwePrivateKey::toLweKey(LweKey &lweKey) const {
		if (static_cast<unsigned int>(lweKey.params->n) != this->length) {
			throw std::logic_error{std::string{"Cannot assign key with length "} + std::to_string(this->length) + " to key with length " + std::to_string(lweKey.params->n)};
		}
		for (unsigned int i = 0; i < (length / 8 + 1); i++) {
			std::byte keyBit{this->keyBits.at(i)};
			for (unsigned int b{}; b < 8; b++) {
				unsigned int keyIndex{8 * i + b};
				if (keyIndex < length) {
					lweKey.key[keyIndex] = static_cast<int32_t>(keyBit | static_cast<std::byte>(1 << b));
				}
			}
		}
	}
}