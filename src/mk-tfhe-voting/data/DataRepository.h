#ifndef MK_TFHE_VOTING_DATAREPOSITORY_H
#define MK_TFHE_VOTING_DATAREPOSITORY_H

#include "mk-tfhe-voting/data/model/LwePublicKey.h"
#include "mk-tfhe-voting/data/model/LwePrivateKey.h"
#include "mk-tfhe-voting/data/model/Credentials.h"
#include "mk-tfhe-voting/data/model/VoteExecution.h"
#include "mk-tfhe-voting/data/model/Vote.h"
#include "mk-tfhe-voting/data/model/VoteSample.h"
#include "mk-tfhe-voting/data/model/CounterSample.h"

#include <memory>
#include <boost/function.hpp>
#include <map>
#include <mutex>
#include <openssl/evp.h>

namespace hsr::data {
	using hsr::data::model::LwePublicKey;
	using hsr::data::model::LwePrivateKey;
	using hsr::data::model::Credentials;
	using hsr::data::model::VoteExecution;
	using hsr::data::model::Vote;
	using hsr::data::model::VoteSample;
	using hsr::data::model::CounterSample;

	class DataRepository {
	private:
		sqlite3 *db;

		void setUp();

		int transactionDepth;

		void executeSQL(const std::string &sql);

		const EVP_MD *algo;

		std::string generateHash(const std::vector<std::shared_ptr<MKLweSample>> &voteSamples);

		std::recursive_mutex mutex;
	public:
		explicit DataRepository(std::string dbname = std::string{"data"});

		virtual ~DataRepository();

		template<typename value_type>
		value_type store(value_type &value) {
			std::vector<value_type> temp{value};
			storeAll(temp);
			return temp.at(0);
		}

		void storeAll(std::vector<LwePublicKey> &lwePublicKeys);

		std::vector<LwePublicKey> findAllLwePublicKeys();

		void storeAll(std::vector<LwePrivateKey> &lwePrivateKeys);

		std::vector<LwePrivateKey> findAllLwePrivateKeys();

		void storeAll(std::vector<Credentials> &credentials);

		std::vector<Credentials> findAllCredentials();

		void storeAll(std::vector<VoteExecution> &credentials);

		std::vector<VoteExecution> findAllVoteExecutions();

		void storeAll(std::vector<Vote> &credentials);

		std::vector<Vote> findAllVotes();

		std::vector<Vote> findVotesByHash(const std::string &hash);

		void persistVote(unsigned long id, const std::vector<std::shared_ptr<MKLweSample>> &voteSamples);

		std::vector<unsigned long> getRecivedVoteIds();

		void setVoteReady(unsigned long);

		std::map<unsigned long, std::vector<std::shared_ptr<MKLweSample>>> getReadyVotes(std::shared_ptr<LweParams> LWEparams, std::shared_ptr<MKTFHEParams> MKparams);

		void setVoteProcessed(unsigned long);

		void storeAll(std::vector<VoteSample> &credentials);

		std::vector<VoteSample> findAllVoteSamples();

		void storeAll(std::vector<CounterSample> &credentials);

		std::vector<CounterSample> findAllCounterSamples();

		void transactionBegin();

		void transactionCommit();

		void transactionRollback();
	};
}

#endif //MK_TFHE_VOTING_DATAREPOSITORY_H
