DROP TABLE IF EXISTS lwe_public_key;
CREATE TABLE IF NOT EXISTS lwe_public_key
(
	id         INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	length     INTEGER                           NOT NULL,
	phaseshift BLOB                              NOT NULL,
	phase      INTEGER                           NOT NULL
);

DROP TABLE IF EXISTS lwe_private_key;
CREATE TABLE IF NOT EXISTS lwe_private_key
(
	id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	length  INTEGER                           NOT NULL,
	keybits BLOB                              NOT NULL
);

DROP TABLE IF EXISTS vote_execution;
CREATE TABLE IF NOT EXISTS vote_execution
(
	id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	orderid INTEGER                           NOT NULL,
	choices INTEGER                           NOT NULL
);

DROP TABLE IF EXISTS credentials;
CREATE TABLE IF NOT EXISTS credentials
(
	id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	componentid INTEGER                           NOT NULL,
	apikey      TEXT                              NOT NULL
);

DROP TABLE IF EXISTS vote;
CREATE TABLE IF NOT EXISTS vote
(
	id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	external_id INTEGER                           NOT NULL,
	ready       INTEGER                           NOT NULL,
	processed   INTEGER                           NOT NULL,
	hash        TEXT                              NOT NULL
);

DROP TABLE IF EXISTS vote_sample;
CREATE TABLE IF NOT EXISTS vote_sample
(
	id                INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	vote_id           INTEGER                           NOT NULL,
	vote_execution_id INTEGER                           NOT NULL,
	order_id          INTEGER                           NOT NULL,
	lwe_public_key_id INTEGER                           NOT NULL
);

DROP TABLE IF EXISTS counter_sample;
CREATE TABLE IF NOT EXISTS counter_sample
(
	id                INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	vote_execution_id INTEGER                           NOT NULL,
	choice_id         INTEGER                           NOT NULL,
	order_id          INTEGER                           NOT NULL,
	lwe_public_key_id INTEGER                           NOT NULL
);
