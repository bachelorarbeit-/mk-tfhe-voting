#include "DataRepositoryHelpers.h"
#include <ostream>

namespace hsr::data::helpers {
	int callback(void *pointer, int length, char **values, char **) {
		if (length > 0 && values[0] != nullptr) {
			long *id = static_cast<long *>(pointer);
			*id = std::stol(values[0]);
		}
		return 0;
	}

	long getLastRowId(sqlite3 *db, const std::string &table) {
		char *zErrMsg = nullptr;
		const std::string sql{"SELECT MAX(ID) as ID FROM " + table};
		long id = 0;
		int rc = sqlite3_exec(db, sql.c_str(), callback, &id, &zErrMsg);

		if (rc != SQLITE_OK) {
			std::string error{"SQL error: "};
			error += zErrMsg;
			sqlite3_free(zErrMsg);
			throw std::runtime_error{error};
		}
		return id;
	}

	namespace to_stream {
		std::ostream &operator<<(std::ostream &os, std::shared_ptr<MKLweSample> &mkLweSample) {
			os << mkLweSample->n << mkLweSample->parties << mkLweSample->b;
			for (unsigned int i{0}; i < static_cast<unsigned int>(mkLweSample->n * mkLweSample->parties); i++) {
				os << mkLweSample->a[i];
			}
			return os;
		}
	}
}