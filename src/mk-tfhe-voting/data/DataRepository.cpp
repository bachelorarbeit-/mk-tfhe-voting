#include "database.h"

#include "../exceptions/DataException.h"

#include <sqlite3.h>
#include <stdexcept>
#include "DataRepository.h"
#include "DataRepositoryHelpers.h"
#include "mk-tfhe-voting/data/model/VoteExecution.h"

#include <mkTFHEsamples.h>
#include <functional>
#include <iomanip>
#include <thread>
#include <iostream>


namespace hsr::data {
	DataRepository::DataRepository(std::string dbname) : db{}, transactionDepth{0}, algo{EVP_sha3_512()}, mutex{} {
		int rc = sqlite3_open((dbname + ".db").c_str(), &this->db);

		if (rc) {
			throw std::runtime_error{std::string{"Can't open database: "} + sqlite3_errmsg(this->db)};
		}
		this->setUp();
		if (algo == nullptr) {
			throw DataException{"sha3_512 is not available"};
		}
	}

	//database.h is generated from => cat database.sql | sed -e's/  */ /g' | sed -e's/[\t]*//g' > min.sql && xxd -i min.sql | sed -e's/min_sql/database_sql/g' > database.h && rm min.sql
	void DataRepository::setUp() {
		char *zErrMsg = nullptr;
		std::string sql{reinterpret_cast<char *>(database_sql), database_sql_len};
		int rc = sqlite3_exec(this->db, sql.c_str(), nullptr, nullptr, &zErrMsg);

		if (rc != SQLITE_OK) {
			std::string error{"SQL error in setup: "};
			error += zErrMsg;
			sqlite3_free(zErrMsg);
			throw DataException{error};
		}
	}

	DataRepository::~DataRepository() {
		sqlite3_close_v2(this->db);
	}

	void DataRepository::transactionBegin() {
		if (this->transactionDepth++ == 0) {
			char *errorMessage = nullptr;

			sqlite3_exec(db, "BEGIN TRANSACTION", nullptr, nullptr, &errorMessage);

			if (errorMessage != nullptr) {
				std::string error{"SQL error in while starting transaction: "};
				error += sqlite3_errmsg(db);
				sqlite3_free(errorMessage);
				throw DataException{error};
			}
		}
	}

	void DataRepository::transactionCommit() {
		if (--this->transactionDepth == 0) {
			char *errorMessage = nullptr;

			sqlite3_exec(db, "COMMIT TRANSACTION", nullptr, nullptr, &errorMessage);

			if (errorMessage != nullptr) {
				std::string error{"SQL error while commiting: "};
				error += sqlite3_errmsg(db);
				sqlite3_free(errorMessage);
				sqlite3_exec(db, "ROLLBACK TRANSACTION", nullptr, nullptr, &errorMessage);
				this->transactionDepth = 0;
				throw DataException{error};
			}
			sqlite3_db_cacheflush(db);
		} else if (this->transactionDepth < 0) {
			this->transactionDepth = 0;

			throw DataException{"DataRepository::transactionCommit was called more often than DataRepository::transactionBegin"};
		}
	}

	void DataRepository::transactionRollback() {
		if (this->transactionDepth > 0) {
			this->transactionDepth = 0;
			char *errorMessage = nullptr;

			sqlite3_exec(db, "ROLLBACK TRANSACTION", nullptr, nullptr, &errorMessage);

			if (errorMessage != nullptr) {
				std::string error{"SQL error in rollback: "};
				error += sqlite3_errmsg(db);
				sqlite3_free(errorMessage);
				throw DataException{error};
			}
		}
	}

	void DataRepository::executeSQL(const std::string &sql) {
		try {
			transactionBegin();

			char *zErrMsg = nullptr;
			int rc = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &zErrMsg);

			if (rc != SQLITE_OK) {
				std::string error{"SQL error while executing statement: "};
				error += zErrMsg;
				sqlite3_free(zErrMsg);
				throw DataException{error};
			}

			transactionCommit();
		} catch (DataException &e) {
			transactionRollback();
			throw e;
		}
	}

	void DataRepository::storeAll(std::vector<LwePublicKey> &lwePublicKeys) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, LwePublicKey &value, long id)> callback = [](sqlite3_stmt *stmt, LwePublicKey &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int(stmt, 2, static_cast<int>(value.getLength()));
			sqlite3_bind_blob(stmt, 3, value.getPhaseShift().data(), static_cast<int>(value.getPhaseShift().size() * sizeof(int)), SQLITE_STATIC);
			sqlite3_bind_int(stmt, 4, value.getPhase());
		};
		helpers::storeVector(*this, this->db, lwePublicKeys, "lwe_public_key", std::vector<std::string>{"id", "length", "phaseshift", "phase"}, callback);
	}

	std::vector<LwePublicKey> DataRepository::findAllLwePublicKeys() {
		std::scoped_lock lock{this->mutex};
		std::vector<LwePublicKey> results;

		std::function<LwePublicKey(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id = sqlite3_column_int64(statement, 0);
			unsigned int length = sqlite3_column_int(statement, 1);

			int blobLength = sqlite3_column_bytes(statement, 2);
			const int *blobPointer = static_cast<const int *>(sqlite3_column_blob(statement, 2));
			std::vector<int> phaseShift{blobPointer, blobPointer + blobLength / sizeof(int)};

			int phase = sqlite3_column_int(statement, 3);
			return LwePublicKey(id, length, phaseShift, phase);
		};
		helpers::findAll(this->db, results, "lwe_public_key", callback);

		return results;
	}

	void DataRepository::storeAll(std::vector<LwePrivateKey> &lwePrivateKeys) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, LwePrivateKey &value, long id)> callback = [](sqlite3_stmt *stmt, LwePrivateKey &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int(stmt, 2, value.getLength());
			sqlite3_bind_blob(stmt, 3, value.getKeyBits().data(), static_cast<int>(value.getKeyBits().size() * sizeof(std::byte)), SQLITE_STATIC);
		};
		helpers::storeVector(*this, this->db, lwePrivateKeys, "lwe_private_key", std::vector<std::string>{"id", "length", "keybits"}, callback);
	}

	std::vector<LwePrivateKey> DataRepository::findAllLwePrivateKeys() {
		std::scoped_lock lock{this->mutex};
		std::vector<LwePrivateKey> results;

		std::function<LwePrivateKey(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id = sqlite3_column_int64(statement, 0);
			unsigned int length = sqlite3_column_int(statement, 1);

			int blobLength = sqlite3_column_bytes(statement, 2);
			const std::byte *blobPointer{static_cast<const std::byte *>(sqlite3_column_blob(statement, 2))};
			std::vector<std::byte> phaseShift{blobPointer, blobPointer + blobLength / sizeof(std::byte)};

			return LwePrivateKey(id, length, phaseShift);
		};
		helpers::findAll(this->db, results, "lwe_private_key", callback);

		return results;
	}

	void DataRepository::storeAll(std::vector<Credentials> &credentials) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, Credentials &value, long id)> callback = [](sqlite3_stmt *stmt, Credentials &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int64(stmt, 2, value.getComponentId());
			sqlite3_bind_text(stmt, 3, value.getApikey().c_str(), value.getApikey().size(), SQLITE_STATIC);
		};
		helpers::storeVector(*this, this->db, credentials, "credentials", std::vector<std::string>{"id", "componentid", "apikey"}, callback);
	}

	std::vector<Credentials> DataRepository::findAllCredentials() {
		std::scoped_lock lock{this->mutex};
		std::vector<Credentials> results;

		std::function<Credentials(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long componentId{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			std::string apikey{reinterpret_cast<const char *>(sqlite3_column_text(statement, 2))};

			return Credentials(id, componentId, apikey);
		};
		helpers::findAll(this->db, results, "credentials", callback);

		return results;
	}

	void DataRepository::storeAll(std::vector<VoteExecution> &voteExecutions) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, VoteExecution &value, long id)> callback = [](sqlite3_stmt *stmt, VoteExecution &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int64(stmt, 2, value.getOrderId());
			sqlite3_bind_int64(stmt, 3, value.getChoices());
		};
		helpers::storeVector(*this, this->db, voteExecutions, "vote_execution", std::vector<std::string>{"id", "orderid", "choices"}, callback);
	}

	std::vector<VoteExecution> DataRepository::findAllVoteExecutions() {
		std::scoped_lock lock{this->mutex};
		std::vector<VoteExecution> results;

		std::function<VoteExecution(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long orderid{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			unsigned long choices{static_cast<unsigned long>(sqlite3_column_int64(statement, 2))};

			return VoteExecution(id, orderid, choices);
		};
		helpers::findAll(this->db, results, "vote_execution", callback);

		return results;
	}

	void DataRepository::storeAll(std::vector<Vote> &votes) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, Vote &value, long id)> callback = [](sqlite3_stmt *stmt, Vote &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int64(stmt, 2, value.getExternalId());
			sqlite3_bind_int(stmt, 3, value.isReady());
			sqlite3_bind_int(stmt, 4, value.isProcessed());
			sqlite3_bind_text(stmt, 5, value.getHash().c_str(), value.getHash().length(), SQLITE_STATIC);
		};
		helpers::storeVector(*this, this->db, votes, "vote", std::vector<std::string>{"id", "external_id", "ready", "processed", "hash"}, callback);
	}

	std::vector<Vote> DataRepository::findAllVotes() {
		std::scoped_lock lock{this->mutex};
		std::vector<Vote> results;

		std::function<Vote(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long externalId{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			bool ready{static_cast<bool>(sqlite3_column_int(statement, 2))};
			bool processed{static_cast<bool>(sqlite3_column_int(statement, 3))};
			std::string hash{reinterpret_cast<const char *>(sqlite3_column_text(statement, 4))};

			return Vote(id, externalId, ready, processed, hash);
		};
		helpers::findAll(this->db, results, "vote", callback);

		return results;
	}

	std::vector<Vote> DataRepository::findVotesByHash(const std::string &hash) {
		std::scoped_lock lock{this->mutex};
		std::vector<Vote> results;

		std::function<Vote(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long externalId{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			bool ready{static_cast<bool>(sqlite3_column_int(statement, 2))};
			bool processed{static_cast<bool>(sqlite3_column_int(statement, 3))};
			std::string hash{reinterpret_cast<const char *>(sqlite3_column_text(statement, 4))};

			return Vote(id, externalId, ready, processed, hash);
		};
		std::string select{sqlite3_mprintf("SELECT * FROM vote WHERE hash=%Q", hash.c_str())};
		helpers::select(this->db, select, results, callback);

		return results;
	}

	void DataRepository::persistVote(unsigned long id, const std::vector<std::shared_ptr<MKLweSample>> &voteSamples) {
		std::scoped_lock lock{this->mutex};
		try {
			this->transactionBegin();
			Vote vote{0, id, false, false, this->generateHash(voteSamples)};
			vote = this->store(vote);

			std::vector<LwePublicKey> lweSamples{};
			for (const std::shared_ptr<MKLweSample> &sample : voteSamples) {
				lweSamples.emplace_back(LwePublicKey{*sample});
			}
			this->storeAll(lweSamples);

			std::vector<VoteSample> samples{};
			unsigned int order{0};
			for (LwePublicKey &sample : lweSamples) {
				samples.emplace_back(0, vote.getId(), 0, order++, sample.getId());
			}
			this->storeAll(samples);
			this->transactionCommit();
		} catch (DataException &e) {
			this->transactionRollback();
			throw e;
		}
	}

	std::vector<unsigned long> DataRepository::getRecivedVoteIds() {
		std::scoped_lock lock{this->mutex};
		std::vector<unsigned long> ids{};

		std::function<unsigned long(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			return static_cast<unsigned long>(sqlite3_column_int64(statement, 0));
		};

		helpers::select(this->db, std::string{"SELECT v.external_id FROM vote as v WHERE v.ready = 0"}, ids, callback);

		return ids;
	}

	void DataRepository::setVoteReady(unsigned long id) {
		std::scoped_lock lock{this->mutex};
		const std::string sql{"UPDATE vote SET ready = 1 WHERE external_id=" + std::to_string(id)};
		executeSQL(sql);
	}

	std::map<unsigned long, std::vector<std::shared_ptr<MKLweSample>>> DataRepository::getReadyVotes(std::shared_ptr<LweParams> LWEparams, std::shared_ptr<MKTFHEParams> MKparams) {
		std::scoped_lock lock{this->mutex};
		std::map<unsigned long, std::vector<std::shared_ptr<MKLweSample>>> votes{};

		sqlite3_stmt *statement;
		const std::string sql{
				"SELECT v.external_id, lpk.* FROM vote as v JOIN vote_sample as vs ON (v.id  = vs.vote_id) JOIN lwe_public_key as lpk ON (vs.lwe_public_key_id = lpk.id) WHERE v.ready = 1 AND v.processed = 0 ORDER BY vs.order_id ASC;"};
		if (sqlite3_prepare_v2(db, sql.c_str(), sql.length(), &statement, 0) != SQLITE_OK) {
			throw DataException{sql + " failed"};
		}

		while (sqlite3_step(statement) == SQLITE_ROW) {
			unsigned long voteId{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};

			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			unsigned int length{static_cast<unsigned int>(sqlite3_column_int(statement, 2))};

			int blobLength{sqlite3_column_bytes(statement, 3)};
			const int *blobPointer{static_cast<const int *>(sqlite3_column_blob(statement, 3))};
			std::vector<int> phaseShift{blobPointer, blobPointer + blobLength / sizeof(int)};

			int phase{sqlite3_column_int(statement, 4)};


			if (votes.count(voteId) == 0) {
				votes.insert(std::pair<unsigned long, std::vector<std::shared_ptr<MKLweSample>>>{voteId, std::vector<std::shared_ptr<MKLweSample>>{}});
			}
			votes.at(voteId).emplace_back((LwePublicKey(id, length, phaseShift, phase)).toMKLweSample(LWEparams.get(), MKparams.get()));
		}

		return votes;
	}

	void DataRepository::setVoteProcessed(unsigned long id) {
		std::scoped_lock lock{this->mutex};
		const std::string sql{"UPDATE vote SET processed = 1 WHERE external_id=" + std::to_string(id)};
		executeSQL(sql);
	}

	void DataRepository::storeAll(std::vector<VoteSample> &voteSamples) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, VoteSample &value, long id)> callback = [](sqlite3_stmt *stmt, VoteSample &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int64(stmt, 2, value.getVoteId());
			sqlite3_bind_int64(stmt, 3, value.getVoteExecutionId());
			sqlite3_bind_int(stmt, 4, static_cast<int>(value.getOrderId()));
			sqlite3_bind_int64(stmt, 5, value.getLwePublicKeyId());
		};
		helpers::storeVector(*this, this->db, voteSamples, "vote_sample", std::vector<std::string>{"id", "vote_id", "vote_execution_id", "order_id", "lwe_public_key_id"}, callback);
	}

	std::vector<VoteSample> DataRepository::findAllVoteSamples() {
		std::scoped_lock lock{this->mutex};
		std::vector<VoteSample> results;

		std::function<VoteSample(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long voteId{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			unsigned long executionId{static_cast<unsigned long>(sqlite3_column_int64(statement, 2))};
			unsigned long lwePubKeyId{static_cast<unsigned long>(sqlite3_column_int64(statement, 3))};
			unsigned int orderId{static_cast<unsigned int>(sqlite3_column_int(statement, 4))};

			return VoteSample(id, voteId, executionId, lwePubKeyId, orderId);
		};
		helpers::findAll(this->db, results, "vote_sample", callback);

		return results;
	}

	void DataRepository::storeAll(std::vector<CounterSample> &counterSamples) {
		std::scoped_lock lock{this->mutex};
		std::function<void(sqlite3_stmt *stmt, CounterSample &value, long id)> callback = [](sqlite3_stmt *stmt, CounterSample &value, long id) {
			value.setId(id);
			sqlite3_bind_int64(stmt, 1, value.getId());
			sqlite3_bind_int64(stmt, 2, value.getVoteExecutionId());
			sqlite3_bind_int64(stmt, 3, value.getChoiceId());
			sqlite3_bind_int(stmt, 4, static_cast<int>(value.getOrderId()));
			sqlite3_bind_int64(stmt, 5, value.getLwePublicKeyId());
		};
		helpers::storeVector(*this, this->db, counterSamples, "counter_sample", std::vector<std::string>{"id", "vote_execution_id", "choice_id", "order_id", "lwe_public_key_id"}, callback);
	}

	std::vector<CounterSample> DataRepository::findAllCounterSamples() {
		std::scoped_lock lock{this->mutex};
		std::vector<CounterSample> results;

		std::function<CounterSample(sqlite3_stmt *stmt)> callback = [](sqlite3_stmt *statement) {
			unsigned long id{static_cast<unsigned long>(sqlite3_column_int64(statement, 0))};
			unsigned long executionId{static_cast<unsigned long>(sqlite3_column_int64(statement, 1))};
			unsigned long choiceId{static_cast<unsigned long>(sqlite3_column_int64(statement, 2))};
			unsigned int orderId{static_cast<unsigned int>(sqlite3_column_int(statement, 3))};
			unsigned long lwePubKeyId{static_cast<unsigned long>(sqlite3_column_int64(statement, 4))};

			return CounterSample(id, executionId, choiceId, lwePubKeyId, orderId);
		};
		helpers::findAll(this->db, results, "counter_sample", callback);

		return results;
	}

	std::string DataRepository::generateHash(const std::vector<std::shared_ptr<MKLweSample>> &voteSamples) {
		std::stringstream buffer{};
		for (const std::shared_ptr<MKLweSample> &bit : voteSamples) {
			using namespace helpers::to_stream;
			buffer << bit;
		}
		std::string input{};
		std::getline(buffer, input);

		EVP_MD_CTX *mdctx{nullptr};
		if ((mdctx = EVP_MD_CTX_create()) == nullptr) {
			throw DataException{"EVP_MD_CTX_create() error"};
		}
		if (EVP_DigestInit_ex(mdctx, algo, nullptr) != 1) {
			EVP_MD_CTX_destroy(mdctx);
			throw DataException{"initialize digest engine failed"};
		}
		if (EVP_DigestUpdate(mdctx, input.c_str(), input.size()) != 1) {
			EVP_MD_CTX_destroy(mdctx);
			throw DataException{"EVP_DigestUpdate() error"};
		}

		unsigned char *digest;
		unsigned int digest_len;
		digest_len = EVP_MD_size(algo);

		if ((digest = (unsigned char *) OPENSSL_malloc(digest_len)) == nullptr) {
			EVP_MD_CTX_destroy(mdctx);
			throw DataException{"OPENSSL_malloc() error"};
		}
		if (EVP_DigestFinal_ex(mdctx, digest, &digest_len) != 1) {
			EVP_MD_CTX_destroy(mdctx);
			OPENSSL_free(digest);
			throw DataException{"EVP_DigestFinal_ex() error"};
		}

		std::stringstream hashBuffer{};
		for (unsigned int i = 0; i < digest_len; i++) {
			hashBuffer << std::setfill('0') << std::setw(2) << std::hex << static_cast<unsigned int>(digest[i]);
		}
		std::string hash{};
		std::getline(hashBuffer, hash);

		OPENSSL_free(digest);
		EVP_MD_CTX_destroy(mdctx);
		return hash;
	}
}