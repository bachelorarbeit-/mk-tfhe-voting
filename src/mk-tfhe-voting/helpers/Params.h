#ifndef MK_TFHE_VOTING_PARAMS_H
#define MK_TFHE_VOTING_PARAMS_H


#include <tfhe.h>
#include <mkTFHEparams.h>

#include <cstdint>
#include <memory>


struct Params {
	// 2 trustees, B=2^7, d=4
	// 4 trustees, B=2^6, d=5
	// 8 trustees, B=2^4, d=8
	// noise 3.29e-10, security level 152 by following TFHE analysis

	int32_t k = 1;

	// new params
	uint32_t Bksbit = 2;          // Base bit key switching
	int32_t ksDimension{8};
	int32_t N = 1024;             // RLWE,RGSW modulus
	int32_t n = 500;

	int32_t gBaseBit{4};
	int32_t gDimension{8};

	double stdevLWE = 0.0078125;           // LWE ciphertexts standard deviation
	double stdevKS = 2.44e-5;              // KS key standard deviation
	double stdevRLWEkey = 3.29e-10;        // 0; // 0.012467;  // RLWE key standard deviation
	double stdevRLWE = 3.29e-10;           // 0; // 0.012467;     // RLWE ciphertexts standard deviation
	double stdevRGSW = 3.29e-10;           // RGSW ciphertexts standard deviation
	double stdevBK = 3.29e-10;             // BK standard deviation

	unsigned generatedKeys{8192};          // total number of LWE public keys per party
	unsigned encryptionKeys{12};           // number of LWE public keys per party added to plaintext bit

	unsigned trustees{3};
	unsigned bulletinBoards{1};
	unsigned choices{4};

	std::shared_ptr<LweParams> lweParams;
	std::shared_ptr<LweParams> lwePubKeyParams;
	std::shared_ptr<TLweParams> tLweParams;
	std::shared_ptr<LweParams> extractedLweParams;
	std::shared_ptr<MKTFHEParams> mkParams;

	explicit Params(int32_t k = 1,
					int32_t n = 500,
					double stdevLwe = 0.0078125,
					uint32_t bksbit = 2,
					int32_t dks = 8,
					double stdevKs = 2.44e-5,
					int32_t N = 1024,
					double stdevRlwEkey = 3.29e-10,
					double stdevRlwe = 3.29e-10,
					double stdevRgsw = 3.29e-10,
					int32_t bgbit = 4,
					int32_t gDimension = 8,
					double stdevBk = 3.29e-10,
					unsigned trustees = 3,
					unsigned generatedKeys = 8192,
					unsigned encryptionKeys = 12,
					unsigned bulletinBoards = 1,
					unsigned choices = 4)
			: k{k}, Bksbit{bksbit}, ksDimension{dks}, N{N}, n{n}, gBaseBit{bgbit}, gDimension{gDimension}, stdevLWE{stdevLwe}, stdevKS{stdevKs}, stdevRLWEkey{stdevRlwEkey}, stdevRLWE{stdevRlwe},
			  stdevRGSW{stdevRgsw}, stdevBK{stdevBk},
			  generatedKeys{generatedKeys}, encryptionKeys{encryptionKeys}, trustees{trustees}, bulletinBoards{bulletinBoards}, choices{choices},
			  lweParams{std::make_shared<LweParams>(n, stdevKs, stdevLwe)},
			  lwePubKeyParams{std::make_shared<LweParams>(n, stdevLWE / sqrt((double) (trustees * encryptionKeys)), stdevLwe)}, tLweParams{std::make_shared<TLweParams>(N, k, stdevBk, stdevLwe)},
			  extractedLweParams{std::make_shared<LweParams>(k * N, stdevKs, stdevLwe)},
			  mkParams{std::make_shared<MKTFHEParams>(n, k * N, 0, stdevLWE, (int32_t) Bksbit, dks, stdevKS, N, 0, stdevRLWEkey, stdevRLWE, stdevRGSW, gBaseBit, gDimension, stdevBK, trustees)} {}

	double getStdevLwePk() const {
		return stdevLWE / sqrt((double) (trustees * encryptionKeys));
	}
};


#endif //MK_TFHE_VOTING_PARAMS_H
