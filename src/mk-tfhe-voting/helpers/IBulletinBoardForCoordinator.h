#ifndef MK_TFHE_VOTING_IBULLETINBOARDFORCOORDINATOR_H
#define MK_TFHE_VOTING_IBULLETINBOARDFORCOORDINATOR_H


struct IBulletinBoardForCoordinator {
	virtual ~IBulletinBoardForCoordinator() = default;

	virtual void generateKeys() = 0;

	virtual void voteDone() = 0;
};


#endif //MK_TFHE_VOTING_IBULLETINBOARDFORCOORDINATOR_H
