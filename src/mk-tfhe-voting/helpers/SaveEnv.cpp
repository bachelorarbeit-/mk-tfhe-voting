#include "SaveEnv.h"

std::string getSaveEnv(std::string env) {
	char const *tmp = getenv(env.c_str());
	if (tmp == nullptr) {
		return std::string{};
	} else {
		return std::string{tmp};
	}
}

std::string getSaveEnv(std::string env, std::string defaultValue) {
	char const *tmp = getenv(env.c_str());
	if (tmp == nullptr) {
		return defaultValue;
	} else {
		return std::string{tmp};
	}
}