//
// Created by Lukas on 23.11.2019.
//

#ifndef MK_TFHE_VOTING_SAVEENV_H
#define MK_TFHE_VOTING_SAVEENV_H

#include <string>

std::string getSaveEnv(std::string env);

std::string getSaveEnv(std::string env, std::string defaultValue);

#endif //MK_TFHE_VOTING_SAVEENV_H
