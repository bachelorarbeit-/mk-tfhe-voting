#ifndef MK_TFHE_VOTING_ICOORDINATORFORBULLETINBOARD_H
#define MK_TFHE_VOTING_ICOORDINATORFORBULLETINBOARD_H


#include "BulletinBoardParams.h"
#include "VotesText.h"
#include <string>
#include <vector>

struct ICoordinatorForBulletinBoard {
	virtual ~ICoordinatorForBulletinBoard() = default;

	virtual void registerBulletinBoardToCoordinator() = 0;

	virtual std::vector<VotesText> getVotes() = 0;

	virtual std::shared_ptr<BulletinBoardParams> registerBulletinBoardToVote(unsigned voteNumber) = 0;

	virtual void signalReady() = 0;

	virtual void trackBallotPaper(unsigned ballotPaperId, std::string const &type) = 0;

	virtual std::vector<unsigned> checkBallotPaperReceived(unsigned ballotPaperId) = 0;

	virtual void publishCounters(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> vector) = 0;
};


#endif //MK_TFHE_VOTING_ICOORDINATORFORBULLETINBOARD_H
