#ifndef MK_TFHE_VOTING_VOTINGPACKAGE_H
#define MK_TFHE_VOTING_VOTINGPACKAGE_H


#include <utility>

#include "IBulletinBoardForVoter.h"
#include "Params.h"
#include "VotesText.h"

#include <iomanip>


struct VotingPackage {

	unsigned ballotId{};

	std::vector<std::weak_ptr<IBulletinBoardForVoter>> bulletinBoards{};

	std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> lwePubKey{};

	const unsigned choices{4};

	const unsigned generatedKeys{8192};          // total number of LWE public keys per party
	const unsigned encryptionKeys{13};           // number of LWE public keys per party added to plaintext bit

	const unsigned trustees{3};

	const std::shared_ptr<LweParams> lweParams;
	const std::shared_ptr<LweParams> lwePubKeyParams;
	const std::shared_ptr<TLweParams> tLweParams;
	const std::shared_ptr<LweParams> extractedLweParams;
	const std::shared_ptr<MKTFHEParams> mkParams;

	VotesText votesText{};

	std::tm voteStart{};
	std::tm voteEnd{};


	VotingPackage(unsigned ballotId, std::vector<std::weak_ptr<IBulletinBoardForVoter>> bulletinboards, std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> lwePubKey, const Params &params,
				  const VotesText votesText, std::tm voteStart, std::tm voteEnd)
			: ballotId{ballotId}, bulletinBoards{std::move(bulletinboards)}, lwePubKey{std::move(lwePubKey)},
			  choices{params.choices}, generatedKeys(params.generatedKeys), encryptionKeys(params.encryptionKeys), trustees(params.trustees), lweParams{params.lweParams},
			  lwePubKeyParams{params.lwePubKeyParams}, tLweParams{params.tLweParams}, extractedLweParams{params.extractedLweParams}, mkParams{params.mkParams},
			  votesText{votesText}, voteStart{voteStart}, voteEnd{voteEnd} {}
};


#endif //MK_TFHE_VOTING_VOTINGPACKAGE_H
