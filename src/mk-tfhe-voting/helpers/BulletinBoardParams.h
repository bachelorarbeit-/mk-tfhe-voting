#ifndef MK_TFHE_VOTING_BULLETINBOARDPARAMS_H
#define MK_TFHE_VOTING_BULLETINBOARDPARAMS_H


#include "ITrusteeForBulletinBoard.h"
#include "Params.h"
#include <vector>
#include <cmath>
#include <cstdint>
#include <tfhe.h>
#include <mkTFHEparams.h>
#include <memory>


struct BulletinBoardParams {
	const unsigned bulletinboardId;
	const unsigned voteId;
	const unsigned generatedKeys{8192};          // total number of LWE public keys per party
	const unsigned encryptionKeys{13};           // number of LWE public keys per party added to plaintext bit

	const unsigned bulletinBoards{1};
	const unsigned choices{4};

	const std::shared_ptr<LweParams> lweParams;
	const std::shared_ptr<LweParams> lwePubKeyParams;
	const std::shared_ptr<TLweParams> tLweParams;
	const std::shared_ptr<LweParams> extractedLweParams;
	const std::shared_ptr<MKTFHEParams> mkParams;

	std::vector<std::weak_ptr<ITrusteeForBulletinBoard>> trustees;
	std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey{};

	BulletinBoardParams(unsigned bulletinboardId, unsigned voteId, std::vector<std::weak_ptr<ITrusteeForBulletinBoard>> trustees, std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey, const Params &params)
			: bulletinboardId{bulletinboardId},
			  voteId{voteId},
			  generatedKeys{params.generatedKeys},
			  encryptionKeys{params.encryptionKeys},
			  bulletinBoards{params.bulletinBoards},
			  choices{params.choices},
			  lweParams{params.lweParams},
			  lwePubKeyParams{params.lwePubKeyParams},
			  tLweParams{params.tLweParams},
			  extractedLweParams{params.extractedLweParams},
			  mkParams{params.mkParams},
			  trustees{std::move(trustees)},
			  rlwePubKey{std::move(rlwePubKey)} {}
};

#endif //MK_TFHE_VOTING_BULLETINBOARDPARAMS_H
