#ifndef MK_TFHE_VOTING_ITRUSTEE_H
#define MK_TFHE_VOTING_ITRUSTEE_H

#include<tfhe.h>

namespace hsr::trustee {
	class ITrustee {
	public:
		virtual Torus32 lwePartialDecrypt(const MKLweSample *sample) = 0;
	};
}


#endif //MK_TFHE_VOTING_ITRUSTEE_H
