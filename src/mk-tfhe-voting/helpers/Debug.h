//
// Created by Lukas on 26.09.2019.
//

#ifndef MK_TFHE_VOTING_DEBUG_H
#define MK_TFHE_VOTING_DEBUG_H

#include <polynomials.h>

void printTorusPolynomial(TorusPolynomial &poly);


#endif //MK_TFHE_VOTING_DEBUG_H
