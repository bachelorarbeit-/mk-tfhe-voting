#ifndef MK_TFHE_VOTING_KEYSWITCHINGKEYS_H
#define MK_TFHE_VOTING_KEYSWITCHINGKEYS_H


#include "mkTFHEsamples.h"
#include <vector>
#include <memory>

struct KeySwitchingKeys {
	std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey{};
	std::vector<std::shared_ptr<MKTGswUESample>> bootstrappingKey{};
};


#endif //MK_TFHE_VOTING_KEYSWITCHINGKEYS_H
