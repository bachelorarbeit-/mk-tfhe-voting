//
// Created by Lukas on 26.09.2019.
//

#include "Debug.h"

#include <iostream>
#include <iomanip>

void printTorusPolynomial(TorusPolynomial &poly) {
	using namespace std;
	int32_t N = poly.N;

	cout << "[" << N << "]";
	cout << setw(12) << poly.coefsT[0];
	cout << setw(12) << poly.coefsT[1] << " ..";
	cout << setw(12) << poly.coefsT[N - 2];
	cout << setw(12) << poly.coefsT[N - 1] << endl;
}