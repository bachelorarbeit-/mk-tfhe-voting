//
// Created by Lukas on 26.09.2019.
//

#include "Encryption.h"

#include <iostream>
#include <iomanip>
#include <random>

#include <mkTFHEsamples.h>
#include <tfhe.h>

namespace hsr::helpers {
	void mySymEncrypt(MKLweSample *result, int32_t message, int32_t m, int32_t M, const std::vector<std::shared_ptr<LweSample>> &lwePubKey) {
		Torus32 _1s8 = modSwitchToTorus32(1, 8);
		uint32_t parties = result->parties;
		uint32_t n = result->n;

		std::random_device rd;
		std::seed_seq seed{rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd()};
		std::mt19937_64 eng(seed);
		std::uniform_int_distribution<unsigned int> dis(0, m - 1);

		// Initialize as noiseless sample
		result->b = message ? _1s8 : -_1s8;
		result->current_variance = 0;
		for (std::size_t i = 0; i < parties * n; i++) {
			result->a[i] = 0;
		}

		// Add encryptionKeys public keys per trustees
		for (std::size_t p = 0; p < parties; p++) {
			uint32_t offset_n = p * n;
			uint32_t offset_m = p * m;

			for (int j = 0; j < M; j++) {
				// Select random LWE public key
				const LweSample &pk = *lwePubKey.at(offset_m + dis(eng));

				// Add the LWE public key
				result->b += pk.b;
				result->current_variance += pk.current_variance;
				for (unsigned int i = 0; i < n; i++) {
					result->a[offset_n + i] += pk.a[i];
				}
			}
		}
	}

	int32_t mySymDecrypt(const MKLweSample *sample, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees) {
		Torus32 phase = sample->b;

		for (std::size_t p = 0; p < static_cast<std::size_t >(sample->parties); p++) {
			phase -= trustees[p]->lwePartialDecrypt(sample);
		}
		return (phase > 0) ? 1 : 0;
	}

	void verifyGate(const char *label, const MKLweSample *sample, int32_t bit, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees, int32_t *error_count, int32_t trial) {
		verifyGate(label, sample, bit, trustees, error_count, trial, true);
	}

	void
	verifyGate(const char *label, const MKLweSample *sample, int32_t bit, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees, int32_t *error_count, int32_t trial, bool doOutput) {
		int32_t out = mySymDecrypt(sample, trustees);

		using namespace std;

		if (doOutput) {
			cout << label << " clear = " << bit << ", decrypted = " << out << " (b =";
			cout << setw(12) << sample->b << ", alpha = " << setprecision(5);
			cout << sqrt(sample->current_variance) << ")" << endl;
		}

		if (out != bit) {
			*error_count += 1;
			if (doOutput) {
				cout << label << " ERROR!!! in trial " << trial << endl;
			}
		}
	}
}