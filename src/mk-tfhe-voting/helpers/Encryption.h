//
// Created by Lukas on 26.09.2019.
//

#ifndef MK_TFHE_VOTING_ENCRYPTION_H
#define MK_TFHE_VOTING_ENCRYPTION_H

#include "ITrustee.h"

#include <mkTFHEsamples.h>

#include <vector>
#include <memory>

namespace hsr::helpers {
	/**
	 * Encrypts a boolean with an LWE multikey
	 */
	void mySymEncrypt(MKLweSample *result, int32_t message, int32_t m, int32_t M, const std::vector<std::shared_ptr<LweSample>> &lwePubKey);

	/**
	 * Decrypts an LWE multikey-encrypted boolean
	 */
	int32_t mySymDecrypt(const MKLweSample *sample, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees);

	void verifyGate(const char *label, const MKLweSample *sample, int32_t bit, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees, int32_t *error_count, int32_t trial);

	void
	verifyGate(const char *label, const MKLweSample *sample, int32_t bit, const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> &trustees, int32_t *error_count, int32_t trial, bool doOutput);
}


#endif //MK_TFHE_VOTING_ENCRYPTION_H
