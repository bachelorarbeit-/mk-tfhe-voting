#ifndef MK_TFHE_VOTING_VOTETRACKINGINFORMATION_H
#define MK_TFHE_VOTING_VOTETRACKINGINFORMATION_H


#include <ctime>
#include <map>
#include <utility>


struct VoteStatusInformation {

	struct BulletinBoard {

		std::map<std::string, bool> status;

		BulletinBoard(bool voteReceived, bool voteDequeued, bool voteProcessed) {
			status.emplace("voteReceived", voteReceived);
			status.emplace("voteDequeued", voteDequeued);
			status.emplace("voteProcessed", voteProcessed);
		}
	};

	struct Voter {

		std::map<std::string, bool> status;

		Voter(bool packageReceived, bool packageSent) {
			status.emplace("packageReceived", packageReceived);
			status.emplace("packageSent", packageSent);
		}
	};

	VoteStatusInformation(unsigned int voteId, bool packageReceived, bool packageSent, std::map<unsigned int, BulletinBoard> bb) :
			voteId{voteId},
			voter{packageReceived, packageSent},
			bb{std::move(std::move(bb))} {}

	unsigned int voteId{};

	Voter voter;

	std::map<unsigned int, BulletinBoard> bb;
};


#endif //MK_TFHE_VOTING_VOTETRACKINGINFORMATION_H
