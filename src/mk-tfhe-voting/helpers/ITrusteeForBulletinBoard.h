#ifndef MK_TFHE_VOTING_ITRUSTEEFORBULLETINBOARD_H
#define MK_TFHE_VOTING_ITRUSTEEFORBULLETINBOARD_H


#include "KeySwitchingKeys.h"
#include <vector>
#include <memory>

struct ITrusteeForBulletinBoard {
	virtual ~ITrusteeForBulletinBoard() = default;

	virtual KeySwitchingKeys getKeys(LweKeySwitchKey *ks, std::shared_ptr<TLweParams> const &tLweParams, std::shared_ptr<MKTFHEParams> const &mkParams) = 0;

	virtual void sendEncryptedCounters(unsigned int bulletinboardId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters) = 0;
};

#endif //MK_TFHE_VOTING_ITRUSTEEFORBULLETINBOARD_H
