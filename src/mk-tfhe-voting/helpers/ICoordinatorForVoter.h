#ifndef MK_TFHE_VOTING_ICOORDINATORFORVOTER_H
#define MK_TFHE_VOTING_ICOORDINATORFORVOTER_H


#include "VotingPackage.h"
#include "VotesInfo.h"
#include "votetrackinginformation.h"

#include <vector>
#include <map>


struct ICoordinatorForVoter {
	virtual ~ICoordinatorForVoter() = default;

	virtual std::map<unsigned int, VotesText> getVotesVoter() = 0;

	virtual std::shared_ptr<VotingPackage> getVotingPackage(unsigned voteId) = 0;

	virtual void registerVoter() = 0;

	virtual std::vector<unsigned int> getParticipations() = 0;

	virtual std::map<unsigned int, VoteStatusInformation> getVoteStatusInformation(std::vector<unsigned int> &voteIds) = 0;

	virtual void trackVoteReceivedPackage(std::vector<unsigned int> const &voteIds) = 0;

	virtual void trackVoteSentVote(std::vector<unsigned int> const &voteIds) = 0;
};


#endif //MK_TFHE_VOTING_ICOORDINATORFORVOTER_H
