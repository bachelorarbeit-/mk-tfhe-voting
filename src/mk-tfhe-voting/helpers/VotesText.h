#ifndef MK_TFHE_VOTING_VOTESTEXT_H
#define MK_TFHE_VOTING_VOTESTEXT_H


#include <utility>
#include <vector>
#include <string>

struct VotesText {
	unsigned int uniqueIdentifier{};
	std::string question{};
	std::vector<std::string> choices{};

	VotesText() = default;

	VotesText(unsigned int uniqueIdentifier, std::string question, std::vector<std::string> choices) :
			uniqueIdentifier{uniqueIdentifier}, question{std::move(question)}, choices{std::move(choices)} {}
};


#endif //MK_TFHE_VOTING_VOTESTEXT_H
