#include "CyclicBarrier.h"

CyclicBarrier::CyclicBarrier(unsigned numThreads) : m_requestsLock{}, m_condition{}, m_numThreads(numThreads), m_counts{0, 0}, m_index(0), m_disabled(false) {

}

void CyclicBarrier::await() {
	std::unique_lock<std::mutex> lock(m_requestsLock);
	if (m_disabled)
		return;

	unsigned currentIndex = m_index;
	++m_counts[currentIndex];

	if (m_counts[currentIndex] < m_numThreads) {
		while (m_counts[currentIndex] < m_numThreads)
			m_condition.wait(lock);
	} else {
		m_index ^= 1; // flip index
		m_counts[m_index] = 0;
		m_condition.notify_all();
	}
}

void CyclicBarrier::unlock() {
	std::unique_lock<std::mutex> lock(m_requestsLock);
	m_disabled = true;
	m_counts[0] = m_numThreads;
	m_counts[1] = m_numThreads;
	m_condition.notify_all();
}