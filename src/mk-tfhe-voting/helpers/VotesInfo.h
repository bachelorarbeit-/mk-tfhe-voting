#ifndef MK_TFHE_VOTING_VOTESINFO_H
#define MK_TFHE_VOTING_VOTESINFO_H


#include <map>
#include <string>

struct VotesInfo {
	std::map<int, std::string> votes{};
};

#endif //MK_TFHE_VOTING_VOTESINFO_H
