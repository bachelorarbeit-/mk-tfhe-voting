#ifndef MK_TFHE_VOTING_IBULLETINBOARDFORTRUSTEE_H
#define MK_TFHE_VOTING_IBULLETINBOARDFORTRUSTEE_H

#include "mkTFHEsamples.h"
#include <vector>

struct IBulletinBoardForTrustee {
	virtual ~IBulletinBoardForTrustee() = default;

	virtual void sendPartialResults(std::vector<std::vector<Torus32>>) = 0;
};


#endif //MK_TFHE_VOTING_IBULLETINBOARDFORTRUSTEE_H
