#ifndef MK_TFHE_VOTING_ICOORDINATORFORTRUSTEE_H
#define MK_TFHE_VOTING_ICOORDINATORFORTRUSTEE_H


#include "VotesInfo.h"
#include "TrusteeParams.h"
#include "VotesText.h"

struct ICoordinatorForTrustee {
	virtual ~ICoordinatorForTrustee() = default;

	virtual void registerTrusteeToCoordinator() = 0;

	virtual std::vector<VotesText> getVotes() = 0;

	virtual std::shared_ptr<TrusteeParams> registerTrusteeToVote(unsigned voteNumber) = 0;

	virtual void publishKeys(std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> pubKey, int32_t n, unsigned voteId) = 0;

	virtual void publishResults(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<Torus32>> partialDecrypt) = 0;
};


#endif //MK_TFHE_VOTING_ICOORDINATORFORTRUSTEE_H
