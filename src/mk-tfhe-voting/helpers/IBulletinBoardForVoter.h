#ifndef MK_TFHE_VOTING_IBULLETINBOARDFORVOTER_H
#define MK_TFHE_VOTING_IBULLETINBOARDFORVOTER_H


#include "mkTFHEsamples.h"
#include <vector>
#include <memory>

struct IBulletinBoardForVoter {
	virtual ~IBulletinBoardForVoter() = default;

	virtual void submitVote(unsigned int id, std::vector<std::shared_ptr<MKLweSample>> vote) = 0;
};

#endif //MK_TFHE_VOTING_IBULLETINBOARDFORVOTER_H
