#ifndef MK_TFHE_VOTING_ITRUSTEEFORCOORDINATOR_H
#define MK_TFHE_VOTING_ITRUSTEEFORCOORDINATOR_H

struct ITrusteeForCoordinator {
	virtual ~ITrusteeForCoordinator() = default;
};

#endif //MK_TFHE_VOTING_ITRUSTEEFORCOORDINATOR_H
