#ifndef MK_TFHE_VOTING_CYCLICBARRIER_H
#define MK_TFHE_VOTING_CYCLICBARRIER_H

#include <mutex>
#include <condition_variable>

/**
 * CyclicBarrier same as the Java CyclicBarrier. Waits for a specified number of threads before releasing all.
 */
class CyclicBarrier {
private:
	std::mutex m_requestsLock;
	std::condition_variable m_condition;
	const unsigned m_numThreads;
	unsigned m_counts[2];
	unsigned m_index;
	bool m_disabled;

public:
	explicit CyclicBarrier(unsigned numThreads);

	CyclicBarrier(const CyclicBarrier &) = delete;

	CyclicBarrier(CyclicBarrier &&) = delete;

	CyclicBarrier &operator=(const CyclicBarrier &) = delete;

	CyclicBarrier &operator=(CyclicBarrier &&) = delete;

	/**
	 * Waits until the barrier is released. If (numThreads - 1) are waiting the next call will release all threads.
	 */
	void await();

	/**
	 * immediately opens the barrier.
	 */
	void unlock();
};


#endif //MK_TFHE_VOTING_CYCLICBARRIER_H
