#ifndef MK_TFHE_VOTING_TRUSTEEPARAMS_H
#define MK_TFHE_VOTING_TRUSTEEPARAMS_H


#include "IBulletinBoardForTrustee.h"
#include "Params.h"

#include <mkTFHEparams.h>
#include <tfhe.h>

#include <utility>
#include <vector>
#include <cmath>
#include <cstdint>
#include <memory>


struct TrusteeParams {
	const int32_t trustees = 2;          // number of trustees

	const unsigned generatedKeys{8192};          // total number of LWE public keys per party
	const unsigned encryptionKeys{13};           // number of LWE public keys per party added to plaintext bit

	const unsigned choices{4};

	const std::shared_ptr<LweParams> lweParams;
	const std::shared_ptr<LweParams> lwePubKeyParams;
	const std::shared_ptr<TLweParams> tLweParams;
	const std::shared_ptr<LweParams> extractedLweParams;
	const std::shared_ptr<MKTFHEParams> mkParams;

	unsigned id{0};
	unsigned voteId{1};
	std::vector<std::shared_ptr<IBulletinBoardForTrustee>> bulletinBoards{};
	std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey{};

	TrusteeParams(unsigned id, unsigned voteId, std::vector<std::shared_ptr<IBulletinBoardForTrustee>> bulletinBoards, std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey, const Params &params)
			: trustees(params.trustees),
			  generatedKeys(params.generatedKeys),
			  encryptionKeys(params.encryptionKeys),
			  choices{params.choices},
			  lweParams{params.lweParams},
			  lwePubKeyParams{params.lwePubKeyParams},
			  tLweParams{params.tLweParams},
			  extractedLweParams{params.extractedLweParams},
			  mkParams{params.mkParams},
			  id{id},
			  voteId{voteId},
			  bulletinBoards{std::move(bulletinBoards)},
			  rlwePubKey{std::move(rlwePubKey)} {}

	double getStdevLwePk() const {
		return mkParams->stdevLWE / sqrt((double) (trustees * encryptionKeys));
	}
};

#endif //MK_TFHE_VOTING_TRUSTEEPARAMS_H
