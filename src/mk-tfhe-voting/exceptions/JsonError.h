#ifndef MK_TFHE_VOTING_JSONERROR_H
#define MK_TFHE_VOTING_JSONERROR_H


#include "Exception.h"

struct JsonError : Exception {
	JsonError(const std::string &message, const exception &cause) : Exception(message, cause) { }

	explicit JsonError(const std::string &message) : Exception(message) { }
};


#endif //MK_TFHE_VOTING_JSONERROR_H
