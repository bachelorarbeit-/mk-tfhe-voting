#include "Exception.h"

#include <utility>

Exception::Exception(std::string message) : trace{std::move(message)} {
}

Exception::Exception(const std::string &message, const exception &cause) : trace{message + "\ncaused by: " + cause.what()} {

}

const char *Exception::what() const noexcept {
	return trace.c_str();
}