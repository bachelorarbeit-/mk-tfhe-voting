#ifndef MK_TFHE_VOTING_BULLETIN_BOARD_ERROR_H
#define MK_TFHE_VOTING_BULLETIN_BOARD_ERROR_H


#include "Exception.h"

struct BulletinBoardError : Exception {
	BulletinBoardError(const std::string &message, const std::exception &cause) : Exception(message, cause) {}

	explicit BulletinBoardError(const std::string &message) : Exception(message) {}
};


#endif //MK_TFHE_VOTING_BULLETIN_BOARD_ERROR_H
