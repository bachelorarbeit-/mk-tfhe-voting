#ifndef MK_TFHE_VOTING_EXCEPTION_H
#define MK_TFHE_VOTING_EXCEPTION_H

#include <stdexcept>
#include <utility>

class Exception : public std::exception {
	std::string trace;

public:
	explicit Exception(std::string message);

	Exception(const std::string &message, const exception &cause);

	const char *what() const noexcept override;
};

#endif //MK_TFHE_VOTING_EXCEPTION_H
