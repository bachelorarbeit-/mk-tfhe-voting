#ifndef MK_TFHE_VOTING_DATAEXCEPTION_H
#define MK_TFHE_VOTING_DATAEXCEPTION_H

#include "Exception.h"

struct DataException : Exception {
	DataException(const std::string &message, const std::exception &cause) : Exception(message, cause) {}

	explicit DataException(const std::string &message) : Exception(message) {}
};

#endif //MK_TFHE_VOTING_DATAEXCEPTION_H
