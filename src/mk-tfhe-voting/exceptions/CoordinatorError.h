#ifndef MK_TFHE_VOTING_COORDINATORERROR_H
#define MK_TFHE_VOTING_COORDINATORERROR_H


#include "Exception.h"

struct CoordinatorError : Exception {
	CoordinatorError(const std::string &message, const std::exception &cause) : Exception(message, cause) { }

	explicit CoordinatorError(const std::string &message) : Exception(message) { }
};


#endif //MK_TFHE_VOTING_COORDINATORERROR_H
