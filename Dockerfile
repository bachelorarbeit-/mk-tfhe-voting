FROM debian:testing
ARG BASE=/opt/trustee
RUN apt-get update
RUN apt-get upgrade --assume-yes
RUN apt-get install --assume-yes cmake git libboost-atomic-dev libboost-thread-dev libboost-system-dev libboost-date-time-dev libboost-regex-dev libboost-filesystem-dev libboost-random-dev libboost-chrono-dev libboost-serialization-dev libwebsocketpp-dev openssl libssl-dev ninja-build libcpprest-dev libfftw3-dev build-essential gcc-9 g++-9 cpp-9
RUN mkdir -p ${BASE}/build
COPY src ${BASE}/src
COPY includes ${BASE}/includes
COPY lib ${BASE}/lib
COPY server ${BASE}/server
COPY CMakeLists.txt ${BASE}/
RUN cd ${BASE}/build && cmake .. -Dno_tests=on -Dcpprestsdk_DIR=/usr/lib/x86_64-linux-gnu/cmake/ && cmake --build -j .
LABEL maintainer="lukas.laetsch@gmail.com"
EXPOSE 42043
ENTRYPOINT ["/opt/trustee/build/server/mk-tfhe-voting-server"]